#!/bin/bash

# Ensure non-interactive apt-get (for tshark)
export DEBIAN_FRONTEND=noninteractive

# Install packages
apt-get update
apt-get install -y iperf iperf3 tshark apache2

## Set default route via CE router
#sudo route delete -net 0.0.0.0/0
#sudo route add -net 0.0.0.0/0 gw 192.168.193.10

# Set route to user/attacker via CE router
sudo route add -net 192.168.191.0/24 gw 192.168.193.10

# Set route to isp/ce link via CE router
sudo route add -net 192.168.192.0/24 gw 192.168.193.10

