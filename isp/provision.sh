#!/bin/bash

# Configure NAT on public interface
iptables -t nat -A POSTROUTING -o swp1 -j MASQUERADE

# Enable BGP daemon and restart routing
sed -i -e 's/bgpd=no/bgpd=yes/' /etc/frr/daemons
systemctl restart frr.service

# Create management VRF for eth0
net add vrf mgmt

# Configure BGP
net add bgp autonomous-system 65000
net add bgp neighbor 192.168.192.20 remote-as external
net add bgp ipv4 unicast neighbor 192.168.192.20 activate
net add bgp ipv4 unicast neighbor 192.168.192.20 default-originate
net add bgp ipv4 unicast network 192.168.191.0/24
net add routing route 192.0.2.1/32 blackhole

# Add default route to DHCP address .2
GW_SWP1=`ip address show swp1 | grep 'inet ' | awk '{print $2}' | sed 's/\(.*\.\).*/\12/'`
net add routing route 0.0.0.0/0 $GW_SWP1

# Show and commit changes
net pending
net commit description Provisioning

