#!/bin/bash

# Stop sFlow daemon
systemctl stop hsflowd.service

# Configure sFlow
mv /etc/hsflowd.conf /etc/hsflowd.conf.bekup
cat > /etc/hsflowd.conf <<EOF
sflow {
  # polling = 30
  polling = 10
  # sampling = 400
  sampling = 100
  # sampling.100M = 100
  sampling.100M = 10
  # sampling.1G = 1000
  sampling.1G = 100
  # sampling.10G = 10000
  sampling.10G = 1000
  # sampling.40G = 40000
  sampling.40G = 4000
  collector {
    ip = 192.168.194.20
    udpport = 6343
  }
}
EOF

# Forward flow samples
iptables -I FORWARD -j NFLOG --nflog-group 1 --nflog-prefix SFLOW

# Enable and start sFlow daemon
systemctl enable hsflowd.service
systemctl start hsflowd.service

# Enable BGP daemon and restart routing
sed -i -e 's/bgpd=no/bgpd=yes/' /etc/frr/daemons
systemctl restart frr.service

# Configure BGP
net add bgp autonomous-system 65001
net add bgp neighbor 192.168.192.10 remote-as external
net add bgp neighbor 192.168.194.20 remote-as internal
net add bgp neighbor 192.168.194.20 allowas-in origin
net add bgp ipv4 unicast neighbor 192.168.192.10 activate
net add bgp ipv4 unicast neighbor 192.168.194.20 activate
net add bgp ipv4 unicast network 192.168.193.0/24

net add routing community-list standard blackhole permit 65535:666
net add routing route-map blackhole-in permit 10
net add routing route-map blackhole-in permit 10 match community blackhole
net add routing route-map blackhole-in permit 10 match ip address prefix-len 32
net add routing route-map blackhole-in permit 10 set ip next-hop 192.0.2.1
net add bgp neighbor 192.168.194.20 route-map blackhole-in in
net add routing route 192.0.2.1/32 blackhole

# Fix BGP port for sFlow neighbor
iptables -t nat -A OUTPUT --destination 192.168.194.20 \
         -p tcp  --dport 179 -j DNAT --to-destination 192.168.194.20:1179

# Create management VRF for eth0
net add vrf mgmt

# Show and commit changes
net pending
net commit description Provisioning

