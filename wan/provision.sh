#!/bin/vbash

source /opt/vyatta/etc/functions/script-template

configure

set interfaces bridge 'br0'
set interfaces ethernet eth1 bridge-group bridge 'br0'
set interfaces ethernet eth1 traffic-policy out 'ce-out'
set interfaces ethernet eth2 bridge-group bridge 'br0'
set interfaces ethernet eth2 traffic-policy out 'isp-out'

set traffic-policy network-emulator ce-out bandwidth '2048'
set traffic-policy network-emulator ce-out network-delay '5'
set traffic-policy network-emulator ce-out packet-loss '1'
set traffic-policy network-emulator isp-out bandwidth '2048'
set traffic-policy network-emulator isp-out network-delay '5'
set traffic-policy network-emulator isp-out packet-loss '1'

commit

run show interfaces
