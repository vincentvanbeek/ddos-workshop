#!/bin/vbash

export BW=$1
export ND=$2
export PL=$3

source /opt/vyatta/etc/functions/script-template

configure

set traffic-policy network-emulator ce-out bandwidth $BW
set traffic-policy network-emulator ce-out network-delay $ND
set traffic-policy network-emulator ce-out packet-loss $PL
set traffic-policy network-emulator isp-out bandwidth $BW
set traffic-policy network-emulator isp-out network-delay $ND
set traffic-policy network-emulator isp-out packet-loss $PL

commit
