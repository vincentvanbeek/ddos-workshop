#!/bin/bash

# Install packages
apt-get update
apt-get install -y iperf iperf3

## Set default route via ISP router
#sudo route delete -net 0.0.0.0/0
#sudo route add -net 0.0.0.0/0 gw 192.168.191.10

# Set route to web server via ISP router
sudo route add -net 192.168.193.0/24 gw 192.168.191.10

