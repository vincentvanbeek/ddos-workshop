# DDoS Mitigation Workshop

## Introduction

This project provides a complete environment to experiment with DDoS Mitigation.

The following products are being deployed:
- Cumulus VX
- sFlow-RT
- VyOS
- Ubuntu


## Diagram

![Diagram](documentation/ddos-workshop.png)


## Prerequisites

A machine capable of running VirtualBox and Vagrant with at least 4 cores and
8 GB of RAM.


## Preparation

Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](https://www.vagrantup.com/downloads.html).

Clone this repository:

```
$ git clone git@gitlab.com:yoeri/ddos-workshop.git
```

## Deployment

Deploy the environment to VirtualBox with Vagrant:

```
$ cd ddos-workshop
$ vagrant up
```


## Usage

Connect to the VM 'user' and check connectivity to the web servers:

```
$ vagrant ssh user
vagrant@user:~$ ping 192.168.193.20
vagrant@user:~$ ping 192.168.193.30
vagrant@user:~$ curl -o tmp http://192.168.193.20
vagrant@user:~$ curl -o tmp http://192.168.193.30
```

Open [http://localhost:8008](http://localhost:8008) to connect to the sFlow monitor.

Check connectivity by connecting to the ISP and CE routers:

```
$ vagrant ssh ce
vagrant@cumulus-ce:mgmt-vrf:~$ sudo su -
root@cumulus-ce:mgmt-vrf:~# net show interface
root@cumulus-ce:mgmt-vrf:~# net show bgp summary
root@cumulus-ce:mgmt-vrf:~# net show route ipv4
```

```
$ vagrant ssh isp
vagrant@cumulus-isp:mgmt-vrf:~$ sudo su -
root@cumulus-isp:mgmt-vrf:~# net show interface
root@cumulus-isp:mgmt-vrf:~# net show bgp summary
root@cumulus-isp:mgmt-vrf:~# net show route ipv4
```

Examine sFlow packets (and restart ddos-blackhole daemon):

```
$ vagrant ssh sflow
vagrant@sflow:~$ sudo su -
root@sflow:~# docker ps
root@sflow:~# docker kill ddos-blackhole
root@sflow:~# sflowtool
root@sflow:~# ddos-blackhole
root@sflow:~# ddos-blackhole-d
```

Monitor incoming packets on the web servers:

```
$ vagrant ssh web1
vagrant@web1:~$ sudo su -
root@web1:~# ifconfig
root@web1:~# tshark -i enp0s8
```

Simulate an attack:

```
$ vagrant ssh attack
vagrant@attack:~$ sudo su -
root@attack:~# ping -f 192.168.193.20
```

Block the attack via the [DDoS Blackhole](http://127.0.0.1:8008/app/ddos-blackhole/html/) app.
See [Screenshots](#screenshots) below.

Change WAN parameters (e.g. 1024 kbps throughput, 10 ms delay, and 0% loss):

```
$ vagrant ssh wan
vagrant@vyos-wan:~$ sudo ./WAN.sh 1024 10 0
```

Test bandwidth with iperf between user and web server:

```
$ vagrant ssh web1
vagrant@web1:~$ iperf -s
```

```
$ vagrant ssh user
vagrant@user:~$ iperf -c 192.168.193.20 -P 4 -d -t 60
```


## Clean up

Remove the environment:

```
$ vagrant destroy -f
```


## Links

Cumulus Networks
- [Try Cumulus VX](https://cumulusnetworks.com/try-for-free/)
- [Monitoring System Statistics and Network Traffic with sFlow](https://docs.cumulusnetworks.com/display/DOCS/Monitoring+System+Statistics+and+Network+Traffic+with+sFlow)
- [Border Gateway Protocol - BGP](https://docs.cumulusnetworks.com/display/DOCS/Border+Gateway+Protocol+-+BGP)

sFlow Blog
- [Cumulus Networks, sFlow and data center automation](https://blog.sflow.com/2014/06/cumulus-networks-sflow-and-data-center.html)
- [Remotely Triggered Black Hole (RTBH) Routing](https://blog.sflow.com/2017/06/remotely-triggered-black-hole-rtbh.html)
- [BGP FlowSpec on white box switch](https://blog.sflow.com/2017/07/bgp-flowspec-on-white-box-switch.html)

sFlow-RT
- [sFlow-RT](https://sflow-rt.com/)

VyOS
- [VyOS](https://www.vyos.io/)


## Screenshots

![Attack](documentation/ddos-attack.png)
![Mitigate](documentation/ddos-mitigate.png)
![Blackholed](documentation/ddos-blackholed.png)
![VirtualBox](documentation/ddos-virtualbox.png)

